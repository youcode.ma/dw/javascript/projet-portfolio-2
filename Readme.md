# Projet Portfolio  

Un site Portfolio est une collection de projets réalisées décrivant et illustrant l’apprentissage ou la carrière d’une personne, son expérience et ses réussites.  
Dans le monde numérique actuel, un site portfolio et github sont sans doute plus important qu'un CV, quel que soit le secteur dans lequel vous travaillez.  
Dans ce projet vous allez créer un site Portfolio pour afficher les différents projets que vous avez réalisé en donnant le lien github de chaque projet.  

## Périmètre  

- Le portfolio doit contenir au moins 4 rubriques( HOME , ABOUT ME , MY WORK , CONTACT) 
- Inserer une étiquette distinctive (logo ou message)
- Créer un wireframe ou mock up en vous basant sur l'objectif du site portfolio et les projets que vous avez réalisé. 
- Développer le site en respectant le wireframe ou mock up réalisé. 
- Pour la partie MY WORK tous les projets doivent avoir un lien github pour consulter le code source de chaque projet realisé.
- Héberger le portfolio en ligne(vous pouvez utiliser le service gratuit de github pages).

Le design n'est pas le plus important, il peut être simple, mais il doit être professionnel  


## Critères d'évaluation

- Le Portfolio sépare complétement la structure (HTML) du design/style (CSS) et javascript  
- Tous les balises div ont un id ou classe  
- Utiliser les balises HTML5 comme header, article, section etc.. pour donner de signification au code HTML  
- Le contenu est responsive et s'affiche dans les differentes écrans (Desktop, Tablet, Mobile) 
- Utiliser intelligemment au moins une des fonctionnalités, en relation avec du javascript, suivantes : modal, slider, popover, tooltips..  
- L'architecture doit être claire exemple :  
```
    project
    +-- README.md
    +-- index.html    
    +-- css
    │   +-- styles.css   
    +-- js
    |   +-- script.js
    +-- img
    |   +-- image.jpg    
    
```
- Le code est bien organisé   
- L'explication du code est demandé 


## Compétences concernés

- Maquetter une application niveau 3  
- Réaliser une interface utilisateur web statique et adaptable niveau 3  
- Développer une interface utilisateur web dynamique Niveau 1  

## A rendre 

Vous devez rendre un lien github de votre projet.  
Dernier Délai: 15/04/2019  